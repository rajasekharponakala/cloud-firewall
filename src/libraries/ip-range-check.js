


// This is a slightly modified version of index.js from https://github.com/danielcompton/ip-range-check
// Thanks to DanielCompton for publishing the package as opensource software under MIT license
 check_many_cidrs = (parsed_addr, kind, range) => {
    if (typeof (range) === "string") {
        return check_single_cidr(parsed_addr, kind, range)
    }
    else if (typeof (range) === "object") //list
    {
        var ip_is_in_range = false;
        for (var i = 0; i < range.length; i++) {
            if (check_single_cidr(parsed_addr, kind, range[i])) {
                ip_is_in_range = true;
                break
            }
        }
        return ip_is_in_range;
    }
}

 check_single_cidr = (parsed_addr, kind, cidr) => {
    try {
        if (cidr.indexOf('/') === -1) {
            var parsed_cidr_as_ip = ipaddr.process(cidr);
            if ((kind === "ipv6") && (parsed_cidr_as_ip.kind() === "ipv6")){
                return (parsed_addr.toNormalizedString() === parsed_cidr_as_ip.toNormalizedString())
            }
            return (parsed_addr.toString() == parsed_cidr_as_ip.toString())
        }
        else {
            var parsed_range = ipaddr.parseCIDR(cidr);
            return parsed_addr.match(parsed_range)
        }
    }
    catch (e) {
        return false
    }
}
