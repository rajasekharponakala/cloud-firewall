/*******************************************************************************

    
    Cloud Firewall - a browser extension/addon that allows users to block connections 
    to sites, pages and web resources (images, videos, etc) hosted in major cloud services
    if the user wishes to do so. 
    Copyright (C) 2019 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/cloud-firewall
*/

// Some sites that are not just hosted in any cloud. 
// If you are reviewing this, please let me know how to update this list
var some_nonbig5domains = [ "eff.org", "fsf.org", "gnu.org","archive.org","startpage.com", "wikipedia.", "debian.org","trisquel.info","puri.sm","pureos.net","parabola.nu"];

// The below is just to speed up, as there's no need for number crunching process 
//   of "Is IP address present in IP ranges" when the domain name just says "{cloud}.com" ! 

// We have this so we can speed up the request handler and not search in whole IP range CIDR blocks for
// for well known hostnames owned by or hosted on these 5 clouds. 
// If you are reviewing this, please let me know how to update this list:

// Instagram is owned by facebook and hosted on AWS..? 
// adding it to both facebook and amazon list so user can expect to block the site when Facebook toggle is ON
// we also need to consider it blocked on normal functionality based on hosted place, so it's on AWS list too.

var knownbig5domains = {};
knownbig5domains.google = ["google.", "googletagmanager.", "doubleclick.", "blogspot.", "youtube.", "googleapis.","googletagservices.","googleadservices.","ytimg.com","ggpht.com","gitlab.com","google-analytics.com"];
knownbig5domains.amazon = ["amazon.", "aws.", "elb.", "s3.","cloudfront.","awsstatic.","instagram.", "amazonaws."];
knownbig5domains.facebook = ["facebook.","fbcdn.", "whatsapp.", "instagram."];
knownbig5domains.apple = ["apple.","icloud."]
knownbig5domains.microsoft = ["microsoft.","bing.", "github.","windows.com","xbox.com"];
knownbig5domains.cloudflare = ["cloudflare."];
 //Like amazon has aws, elb, s3, cloudfront, what does Cloudflare have for their services to identify on the URL?
